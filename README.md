#Comment to stop GitLab deleting this stuff


# CCAPI

Creative Commons API Image Downloader tool, written in Python.

Takes a search term, and downloads images or files relating (if the file is a PDF, it automatically converts all of it's pages to an image).

**Python Library Requirements**

Must be run in Python3

Must have the following libraries installed:

* Pillow

`pip3 install Pillow`

* html2text

`pip3 install html2text`

* pdf2image

`pip3 install pdf2image`

* inkscape

`apt-get install inkscape`

* xmltodict

`pip3 install xmltodict`

* urllib

`pip3 install urllib`

[Note: urllib may already be included as part of other Python libraries and may not require installation]

