#!/usr/bin/env python3
#version 1.3

import requests
import sys
import os
import argparse
import subprocess
from WikimediaDownloader import *
from FileIOA import *

def CreateFolder(FullPath):
	if not os.path.exists(FullPath):
		os.makedirs(FullPath)
	
def CheckFileForURL(TargetURL,Filename):
	Lines = LoadFileLines(Filename,False)
	
	for Line in Lines:	
		if TargetURL in Line:
			return True
			
	return False
	
def AddURLToFile(TargetURL,Filename):
	AppendFileLine(Filename,TargetURL)

def MakeWikimediaCommonsRequest(SearchTerm,Limit,Width):
	TargetUrl = (
	"https://en.wikipedia.org/w/api.php"
	"?action=query"
	"&generator=images"
	"&prop=imageinfo"
	"&gimlimit="+str(Limit)+
	"&redirects=1"
	"&list=search"
	"&srsearch="+urllib.parse.quote(SearchTerm)+
	"&srnamespace=6"
	"&iiprop="+"|".join(["timestamp","user","userid","canonicaltitle","url","size","mime"])+
	"&iiurlwidth="+str(Width)+
	"&format=json")

	return requests.get(TargetUrl).json()

def DownloadImage(TargetFile,TargetOutput,Width=None,NoLicence=False):
	
	TargetFileExt = ExtractEXT(TargetFile).lower()

	if "png" in TargetFileExt or "jpg" in TargetFileExt or "bmp" in TargetFileExt or "pdf" in TargetFileExt or "svg" in TargetFileExt:	
		DownloadWikimediaImage(TargetFile,TargetOutput,Width,NoLicence)
	else:
		raise ValueError("TargetFileExt is \'"+str(TargetFileExt)+"\' and is not a compatible image.")

def UpdateRelative(IncomingString):
	
	if IncomingString is not None:
	
		if bool(re.match(r'^\.[\\/]', IncomingString)):
			return os.path.join(os.path.realpath(os.getcwd()), IncomingString[2:])
		
		if IncomingString == ".":
			return os.path.realpath(os.getcwd())
	
	return IncomingString

def CCAPIFrontEnd(*,SearchTerm,Limit=1,Width=512,Output=".",MustRename=False,NoLicence=False):
	
	Output = UpdateRelative(Output)
	CreateFolder(Output)
	
	print(Output)
	
	Results = MakeWikimediaCommonsRequest(SearchTerm,Limit,Width)
	IterLimit = 0
	
	for Entry in Results["query"]["search"]:
		TargetFile = Entry['title'].replace("File:","")
		
		try:
			if not FileExists(TargetFile):
				print("Downloading "+str(TargetFile)+"...")
				DownloadImage(TargetFile,Output,Width,NoLicence)
				
				if MustRename:
					TempFile = TargetFile.strip().replace(' ', '_')
					
					FileExt = ExtractEXT(TempFile)
					FileName = SearchTerm+"."+FileExt
					RenameFile(os.path.join(Output,TempFile),os.path.join(Output,FileName))
		
		except Exception as e:
			print(e)
			
		IterLimit += 1
		if IterLimit == Limit:
			break

if __name__ == "__main__":	
	
	CLIParser = argparse.ArgumentParser(description="Download images from Wikimedia Commons based on search terms.")	
	CLIParser.add_argument("-s", "--search", help="The search terms to use",required=True,nargs="*")
	CLIParser.add_argument("-l", "--limit", help="The limit of images to download (default=1)",default=1,type=int)
	CLIParser.add_argument("-w", "--width", help="The image width of the image to download in pixels (default=512)",default=512,type=int)
	CLIParser.add_argument("-o", "--output", help="The directory to download the images to (default=./Images)",default="./Images")
	CLIParser.add_argument("-r", "--rename", help="Whether to rename the images to the search terms (default=False)",default=False,type=bool)
	CLIParser.add_argument("-n", "--nolicence", help="Doesn\'t print the licence text on the image, useful if credit has already been attributed (default=False)",default=False,type=bool)
	
	CLIArgs = CLIParser.parse_args()
	
	CCAPIFrontEnd(
					SearchTerm = " ".join(CLIArgs.search),
					Limit = CLIArgs.limit,
					Width = CLIArgs.width,
					Output = CLIArgs.output,
					MustRename = CLIArgs.rename,
					NoLicence=CLIArgs.nolicence
					)
	
	

